﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Insurancedirect.MenuHelpers
{
    public static class MenuHelper
    {
        public static string LayoutHelper(RouteData data, string defaultLayout = "")
        {
            if (data.Values["controller"].ToString() == "Admin")
            {
                return "~/views/shared/_Layout2.cshtml";
            }

            if (data.Values["controller"].ToString() == "Office")
            {
                return "~/views/shared/_Layout2.cshtml";
            }

            return defaultLayout;
        }

        public static string MakeActiveaction(this UrlHelper urlHelper, string action)
        {
            string result = "active";

            string actionName = urlHelper.RequestContext.RouteData.Values["action"].ToString();

            if (!actionName.Equals(action, StringComparison.OrdinalIgnoreCase))
            {
                result = null;
            }

            return result;
        }
    }
}