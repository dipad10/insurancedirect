﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Insurancedirect.ViewModels.Account
{
    public class PolicyVM
    {
        public int SN { get; set; }
        public string DocumentID { get; set; }
        [Required]
        public string Policyno { get; set; }
        public string UserID { get; set; }
        public string Insuredname { get; set; }
        public string ProductID { get; set; }
        public string Productname { get; set; }
        public string Vehicle_Insurance_class { get; set; }
        public string Vehicle_make { get; set; }
        public string Vehicle_model { get; set; }
        public string Vehicle_year { get; set; }
        public string Registeration_number { get; set; }
        public string Engine_number { get; set; }
        public string Chasis_number { get; set; }
        public string Colour { get; set; }
        public string Usage { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public Nullable<decimal> Amount { get; set; }
        public Nullable<System.DateTime> Effective_date { get; set; }
        public Nullable<System.DateTime> Expiry_date { get; set; }
        public string TransGUID { get; set; }
        public string serial_no { get; set; }
        public string Bizoption { get; set; }
        public Nullable<System.DateTime> Createdon { get; set; }
        public string field02 { get; set; }
        public string field03 { get; set; }
        public string field04 { get; set; }
        public string field05 { get; set; }
    }
}