﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Insurancedirect.ViewModels.Account
{
    public class Create_accountVM
    {
        public int SN { get; set; }
        public string UserID { get; set; }
        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Gender { get; set; }
        public string State { get; set; }
       
      
        public string Address { get; set; }
        public string Occupation { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        public string MobileNo1 { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string MobileNo2 { get; set; }
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Compare("Password")]
        public string confirmpassword { get; set; }


    }
}