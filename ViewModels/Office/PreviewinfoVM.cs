﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Insurancedirect.ViewModels.Office
{
    public class PreviewinfoVM
    {
        public theclockVM theclock { get; set; }
        public HopelifeVM hopelife { get; set; }

        public PreviewinfoVM(){
            this.theclock = new theclockVM();
            this.hopelife = new HopelifeVM();
                   }
    }
}