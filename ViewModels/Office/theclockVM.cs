﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Insurancedirect.ViewModels.Office
{
    public class theclockVM
    {
        public int SN { get; set; }


        [Required]
        public string Insuredname { get; set; }
      [Required]
        public string Vehicle_Insurance_class { get; set; }
        [Required]
        public string Vehicle_make { get; set; }
        [Required]
        public string Vehicle_model { get; set; }
        [Required]
        public string Vehicle_year { get; set; }
        [Required]
        public string Registeration_number { get; set; }
        public string Engine_number { get; set; }
        [Required]
        public string Chasis_number { get; set; }
        [Required]
        public string Colour { get; set; }
        [Required]
        public string Usage { get; set; }

        public string Track_comp_name { get; set; }
        public string Track_comp_phone { get; set; }
        public string Track_comp_address { get; set; }
    }
}