﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Insurancedirect.Filters
{

        public class Authorizesession : System.Web.Mvc.ActionFilterAttribute, System.Web.Mvc.IActionFilter
        {
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                if (HttpContext.Current.Session["userid"] == null)
                {
                var url = filterContext.HttpContext.Request.Url.ToString();
                var routeDictionary = new RouteValueDictionary { { "Action", "Login" }, { "Controller", "Account" }, { "returnurl", url } };
                    filterContext.Result = new RedirectToRouteResult(routeDictionary);
                }
                base.OnActionExecuting(filterContext);
            }
        }

    
}