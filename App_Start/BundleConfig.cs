﻿using System.Web;
using System.Web.Optimization;

namespace Insurancedirect
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/jscontent").Include(
                 "~/assets/js/app.js",
                  "~/Scripts/bootstrap.js",
                "~/assets/js/landingv2.js",
                "~/assets/js/auth.js",
                      "~/assets/js/main.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/assets/css/bulma.css",
                      "~/Content/bootstrap.css",
                      "~/assets/css/app.css",
                      "~/assets/css/core_deep-blue.css"));
        }
    }
}
