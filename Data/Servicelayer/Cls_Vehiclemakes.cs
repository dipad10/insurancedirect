﻿using Insurancedirect.Data.Entites;
using Insurancedirect.Data.Othermodules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Insurancedirect.Data.Servicelayer
{
    public class Cls_Vehiclemakes
    {
        Insurancedirect_DBEntities db = new Insurancedirect_DBEntities();
        ModDrive mod = new ModDrive();

        public List<VehMake> Getvehiclemakes()
        {

            return db.VehMakes.ToList();
        }

        public VehMake GetmakebyID(int makeid)
        {
            return db.VehMakes.Where(p => p.MakeID == makeid).FirstOrDefault();
        }

    }
}