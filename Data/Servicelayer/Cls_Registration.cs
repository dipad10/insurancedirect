﻿using Insurancedirect.Data.Entites;
using Insurancedirect.Data.Othermodules;
using Insurancedirect.ViewModels.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Insurancedirect.Data.Servicelayer
{
    public class Cls_Registration
    {
        Insurancedirect_DBEntities db = new Insurancedirect_DBEntities();
        ModDrive mod = new ModDrive();



        public Registration GetuserbyID(string id)
        {
            return db.Registrations.Where(p => p.UserID == id).FirstOrDefault();
        }

        public Registration Getuserbyemail(string email)
        {
            return db.Registrations.Where(p => p.Email == email).FirstOrDefault();
        }
        public Registration Authenticatelogin(LoginVM login)
        {
            var hashpwd = new ModDrive().Encrypt(login.Password);

            return db.Registrations.Where(p => p.Email == login.Email && p.Password == hashpwd).FirstOrDefault();
        }

        public ModDrive.Response InsertorUpdateUser(Create_accountVM vm)
        {
            if(vm.SN >0)
            {
                //update
                return ModDrive._GetResponseStruct(ModDrive.ErrCodeEnum.NO_ERROR, 1, 0);
            }
            else
            {
                try
                {
                    //insert user
                    Registration reg = new Registration();
                    reg.UserID = "USER"+ DateTime.Now.Year + new Cls_Autosettings().Getnextvalue("UserID");
                    reg.Title = vm.Title;
                    reg.State = vm.State;
                    reg.Occupation = vm.Occupation;
                    reg.MobileNo2 = vm.MobileNo2;
                    reg.MobileNo1 = vm.MobileNo1;
                    reg.Lastname = vm.Lastname;
                    reg.Firstname = vm.Firstname;
                    reg.Email = vm.Email;
                    reg.CreatedOn = DateTime.Now;
                    reg.Address = vm.Address;
                    reg.Gender = vm.Gender;
                    reg.Active = 0;
                    reg.IsVerified = 0;
                    reg.Password = mod.Encrypt(vm.Password);
                    db.Registrations.Add(reg);
                    db.SaveChanges();
                    return ModDrive._GetResponseStruct(ModDrive.ErrCodeEnum.NO_ERROR, 1, 0);
                }
                catch (Exception ex)
                {

                    return ModDrive._GetResponseStruct(ModDrive.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
                }
              
            }
        }

    }
}