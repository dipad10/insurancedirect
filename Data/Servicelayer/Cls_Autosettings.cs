﻿using Insurancedirect.Data.Entites;
using Insurancedirect.Data.Othermodules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Insurancedirect.Data.Servicelayer
{
    public class Cls_Autosettings
    {
        Insurancedirect_DBEntities db = new Insurancedirect_DBEntities();
        ModDrive mod = new ModDrive();

        public  string Getnextvalue(string numbertype)
        {
            string autonumber = "";
            var current = db.Autosettings.FirstOrDefault(a => a.NumberType == numbertype);
            autonumber = current.Nextvalue;
            //call function to add 1 to the number and save for next usage
            Updateautonumber(numbertype);

            return autonumber;
        }

        public void Updateautonumber(string numbertype)
        {
            var current = db.Autosettings.FirstOrDefault(a => a.NumberType == numbertype);
            current.Nextvalue = current.Nextvalue + 1;

            Autosetting auto = new Autosetting
            {
                Nextvalue = current.Nextvalue
            };
            db.SaveChanges();


        

        }
    }
}