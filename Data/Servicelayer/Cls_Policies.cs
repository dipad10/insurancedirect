﻿using Insurancedirect.Data.Entites;
using Insurancedirect.Data.Othermodules;
using Insurancedirect.ViewModels.Office;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Insurancedirect.Data.Servicelayer
{
    public class Cls_Policies
    {
        Insurancedirect_DBEntities db = new Insurancedirect_DBEntities();
        ModDrive mod = new ModDrive();

        public List<Policy> Getpolicies()
        {
            return db.Policies.ToList();
        }

        public Policy GetpolicybyID(string docid)
        {
            return db.Policies.Where(p => p.DocumentID == docid).FirstOrDefault();
        }
        public Policy GetpolicybyPolicyno(string polyno)
        {
            return db.Policies.Where(p => p.Policyno == polyno).FirstOrDefault();
        }
        public List<Policy> Getpolicybyuser(string userid)
        {
            return db.Policies.Where(p => p.UserID == userid).ToList();
        }


        public ModDrive.Response Inserttheclockpolicy(theclockVM vm)
        {
            if (vm.SN > 0)
            {
                //update
                return ModDrive._GetResponseStruct(ModDrive.ErrCodeEnum.NO_ERROR, 1, 0);
            }
            else
            {
                try
                {
                    //insert user
                    Policy pol = new Policy();
                    pol.UserID = mod.Getcurrentuser();
                    pol.DocumentID = Guid.NewGuid().ToString();
                    pol.Policyno = "";
                    pol.ProductID = "TC";
                    pol.Productname = "THECLOCK";
                    pol.Registeration_number = vm.Registeration_number;
                    pol.Usage = vm.Usage;
                    pol.Amount = 5000;
                    pol.Bizoption = "NEW";
                    pol.Chasis_number = vm.Chasis_number;
                    pol.Colour = vm.Colour;
                    pol.Createdon = DateTime.Now;
                    pol.Effective_date = DateTime.Now;
                    pol.Engine_number = vm.Engine_number;
                    pol.Expiry_date = DateTime.Now.AddYears(1).AddDays(-1);
                    pol.Insuredname = vm.Insuredname;
                    pol.Vehicle_Insurance_class = vm.Vehicle_Insurance_class;
                    pol.Vehicle_make = vm.Vehicle_make;
                    pol.Vehicle_model = vm.Vehicle_model;
                    pol.Vehicle_year = vm.Vehicle_year;
                    pol.TransGUID = Guid.NewGuid().ToString();
                    pol.Status = "Awaitimg confirmation";
                    pol.Track_comp_name = vm.Track_comp_name;
                    pol.Track_comp_address = vm.Track_comp_address;
                    pol.Track_comp_phone = vm.Track_comp_phone;

                    db.Policies.Add(pol);
                    db.SaveChanges();
                    return ModDrive._GetResponseStruct(ModDrive.ErrCodeEnum.NO_ERROR, 1, 0);
                }
                catch (Exception ex)
                {

                    return ModDrive._GetResponseStruct(ModDrive.ErrCodeEnum.GENERIC_ERROR, 0, 1, ex.Message);
                }

            }
        }
    }
}