﻿using Insurancedirect.Data.Entites;
using Insurancedirect.Data.Othermodules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Insurancedirect.Data.Servicelayer
{
    public class Cls_Vehiclemakebrands
    {
        Insurancedirect_DBEntities db = new Insurancedirect_DBEntities();
        ModDrive mod = new ModDrive();

        public List<VehMakeBrand> GetvehiclebrandByMake(string make)
        {

            return db.VehMakeBrands.Where(p => p.Make == make).ToList();
        }

        public List<VehMakeBrand> Getallbrands()
        {
            return db.VehMakeBrands.ToList();
        }
    }
}