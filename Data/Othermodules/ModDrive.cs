﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Insurancedirect.Data.Othermodules

{
    public class ModDrive
    {
        #region RESPONSES
        public enum ErrCodeEnum
        {
            NO_ERROR = 0,
            INVALID_GUID = 200,
            INVALID_PASSWORD = 200,
            FORBIDDEN = 407,
            INVALID_RANGE = 408,
            ACCOUNT_LOCKED = 400,
            ACCOUNT_EXPIRED = 400,
            GENERIC_ERROR = 100
        }

        public class Response
        {
            public int ErrorCode;
            public string ErrorMessage;
            public string ExtraMessage;
            public int TotalSuccess;
            public int TotalFailure;
     
        }

        public static Response _GetResponseStruct(ErrCodeEnum ErrCode, int TotalSuccess = 0, int TotalFailure = 0, string ErrorMsg = "", string ExtraMsg = "")
        {
            Response res = new Response
                {
                ErrorCode = Convert.ToInt32(ErrCode),
                ErrorMessage = ErrorMsg,
                ExtraMessage = ExtraMsg,
                TotalSuccess = TotalSuccess,
                TotalFailure = TotalFailure

            };
   
            return res;
        }
        #endregion
        #region DATA ENCRYPTION

        public string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }


        public string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }


        #endregion

        #region GET FUNCTIONS
        public List<int> Getlistofyears()
        {

            return Enumerable.Range(1995, DateTime.Now.Year - 1994).OrderByDescending(i => i).ToList();
        }

        public string Getcurrentuser()
        {
            return HttpContext.Current.Session["userid"].ToString();
        }
        public string Getcurrentuserfirstname()
        {
            return HttpContext.Current.Session["firstname"].ToString();
        }
        #endregion

        public string PopulateBodyproductsuccessful(string firstname, string producttype)
        {
            string body = string.Empty;
            StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Templates/EmailTemplateProductsuccessful.html"));
            body = reader.ReadToEnd();
            body = body.Replace("{firstname}", firstname);
            body = body.Replace("{producttype}", producttype);
          
            return body;
        }




        public string SendHtmlFormattedEmail(string recepientEmail, string cc, string subject, string body)
        {
            try
            {
                System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UserName"], "Insurancedirect.ng");
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail));
                if (cc != "")
                {
                    string[] CCId = cc.Split(',');

                    foreach (string CCEmail in CCId)
                        // Adding Multiple CC email Id
                        mailMessage.CC.Add(new MailAddress(CCEmail));
                }
                else
                {
                }

                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = ConfigurationManager.AppSettings["UserName"];
                NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                smtp.Send(mailMessage);

                return "success";
            }
            catch (Exception ex)
            {
                return "fail";
            }
        }



    }
}