﻿//using IPolicy.Data.Repository;
using Insurancedirect.Data.Servicelayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Insurancedirect.Data.Othermodules
{
    public class Filldropdownhelper
    {
        public SelectList Fillcarmakes()
        {
            var lstitems = new Cls_Vehiclemakes().Getvehiclemakes();
            var makes = new SelectList(lstitems, "make", "make");
            return makes;

        }
        public List<SelectListItem> Fillstates()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Abia", Value = "Abia" });
            lstitem.Add(new SelectListItem { Text = "Lagos", Value = "Lagos" });
            return lstitem;
        }

        public List<SelectListItem> Filltitles()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Mr", Value = "Mr" });
            lstitem.Add(new SelectListItem { Text = "Mrs", Value = "Mrs" });
            lstitem.Add(new SelectListItem { Text = "Dr", Value = "Dr" });
            lstitem.Add(new SelectListItem { Text = "Hrm", Value = "Hrm" });
            lstitem.Add(new SelectListItem { Text = "Prof", Value = "Prof" });
            return lstitem;
        }

        public List<SelectListItem> FillInsuranceclass()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Private", Value = "Private" });
            lstitem.Add(new SelectListItem { Text = "Commercial", Value = "Commercial" });
            return lstitem;
        }
        public List<SelectListItem> FillInsuredtypes()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Individual", Value = "B" });
            lstitem.Add(new SelectListItem { Text = "Corporate", Value = "A" });
            return lstitem;

        }

        public List<SelectListItem> Fillgender()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Male", Value = "Male" });
            lstitem.Add(new SelectListItem { Text = "Female", Value = "Female" });
            return lstitem;

        }

        public List<SelectListItem> FillTitles()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Mr", Value = "Mr" });
            lstitem.Add(new SelectListItem { Text = "Mrs", Value = "Mrs" });
            lstitem.Add(new SelectListItem { Text = "Hrm", Value = "Hrm" });
            lstitem.Add(new SelectListItem { Text = "Prof", Value = "Prof" });
            lstitem.Add(new SelectListItem { Text = "King", Value = "King" });
            lstitem.Add(new SelectListItem { Text = "Dr", Value = "Dr" });
            return lstitem;

        }

        public List<SelectListItem> FilluserGroups()
        {
            List<SelectListItem> lstitem = new List<SelectListItem>();
            lstitem.Add(new SelectListItem { Text = "Group A", Value = "1" });
            lstitem.Add(new SelectListItem { Text = "Group B", Value = "2" });
            lstitem.Add(new SelectListItem { Text = "Group C", Value = "3" });
            lstitem.Add(new SelectListItem { Text = "Group D", Value = "4" });
            lstitem.Add(new SelectListItem { Text = "Group E", Value = "5" });
            lstitem.Add(new SelectListItem { Text = "Group F", Value = "6" });
            lstitem.Add(new SelectListItem { Text = "Group G", Value = "7" });
            lstitem.Add(new SelectListItem { Text = "Group H", Value = "8" });
            lstitem.Add(new SelectListItem { Text = "Group I", Value = "9" });

            return lstitem;

        }




        //public SelectList fillStates()
        //{
        //    ////var states = new SelectList("", )
        //    ////{

        //    ////}

        //    //var states = new SelectList("")

        //}
    }
}