﻿using Insurancedirect.Data.Othermodules;
using Insurancedirect.Data.Servicelayer;
using Insurancedirect.ViewModels.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Insurancedirect.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Login()
        {
            LoginVM vm = new LoginVM();

            return View(vm);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginVM vm, string returnurl)
        {
            if (ModelState.IsValid)
            {
                var user = new Cls_Registration().Authenticatelogin(vm);
                if(user == null)
                {
                    ViewBag.messagetype = "error";
                    ViewBag.message = "Login details are Incorrect, Pls try again.";
                    return View(vm);
                }
                else
                {
                    Session["userid"] = user.UserID;
                    Session["firstname"] = user.Firstname;
                    if (returnurl != null)
                    {
                        return Redirect(returnurl);
                    }
                    else {
                        return Redirect("/Office/Dashboard");
                    }

                   
                }
            }

            return View(vm);
        }
        public ActionResult Create_account()
        {
         
            ViewBag.titles = new Filldropdownhelper().Filltitles();

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create_account(Create_accountVM vm)
        {
            if (ModelState.IsValid)
            {
                //save data and direct to dashboard
                var res = new Cls_Registration().InsertorUpdateUser(vm);
                if(res.ErrorCode == 0)
                {
                    var user = new Cls_Registration().Getuserbyemail(vm.Email);
                    Session["userid"] = user.UserID;
                    Session["firstname"] = user.Firstname;
                    return Redirect("/Office/Dashboard");
                }
                else
                {
                    ViewBag.messagetype = "error";
                    ViewBag.message = res.ErrorMessage + " " + res.ExtraMessage;
                    ViewBag.states = new Filldropdownhelper().Fillstates();

                    return View(vm);
                }
            }
            ViewBag.states = new Filldropdownhelper().Fillstates();

            return View(vm);
        }
    }
}