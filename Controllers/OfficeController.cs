﻿using Insurancedirect.Data.Entites;
using Insurancedirect.Data.Othermodules;
using Insurancedirect.Data.Servicelayer;
using Insurancedirect.ViewModels.Office;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Insurancedirect.Controllers
{
    [Filters.Authorizesession]
    public class OfficeController : Controller
    {
        // GET: UserHome
        public ActionResult Dashboard()
        {
            return View();
        }

        public ActionResult Theclock()
        {
            theclockVM vm = new theclockVM();
            var insured = new Cls_Registration().GetuserbyID(Session["userid"].ToString());
            vm.Insuredname = insured.Firstname + " " + insured.Lastname;
            vm.Vehicle_Insurance_class = "Third party Only";
            ViewBag.insuranceclass = new Filldropdownhelper().FillInsuranceclass();
            ViewBag.vehiclemakes = new Filldropdownhelper().Fillcarmakes();
            var years = new ModDrive().Getlistofyears();

            ViewBag.years = new SelectList(years);
            return View(vm);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Theclock(theclockVM vm)
        {
            if(ModelState.IsValid)
            {
                //put all data in session and redirect to preview page
                Session["insuredname"] = vm.Insuredname;
                Session["vehiclemake"] = vm.Vehicle_make;
                Session["vehiclemodel"] = vm.Vehicle_model;
                Session["vehicleyear"] = vm.Vehicle_year;
                Session["Vehicle_Insurance_class"] = vm.Vehicle_Insurance_class;
              
                Session["registrationnumber"] = vm.Registeration_number;
                Session["Usage"] = vm.Usage;
                Session["Engine_number"] = vm.Engine_number;
                Session["Colour"] = vm.Colour;
                Session["Chasis_number"] = vm.Chasis_number;
                Session["track_comp_name"] = vm.Track_comp_name;
                Session["track_comp_address"] = vm.Track_comp_address;
                Session["track_comp_phone"] = vm.Track_comp_phone;
                Session["producttype"] = "TC";
                return Redirect("/Office/Previewinfo");
               
            }
            var insured = new Cls_Registration().GetuserbyID(Session["userid"].ToString());
            vm.Insuredname = insured.Firstname + " " + insured.Lastname;
            ViewBag.insuranceclass = new Filldropdownhelper().FillInsuranceclass();
            ViewBag.vehiclemakes = new Filldropdownhelper().Fillcarmakes();
            var years = new ModDrive().Getlistofyears();

            ViewBag.years = new SelectList(years);
            return View(vm);
        }

        public ActionResult Previewinfo()
        {


            // pass productype querystring to viewbag for view
            string type = Session["producttype"].ToString();
            ViewBag.producttype = type;
             switch (type)
                {
                    case "TC":
                        PreviewinfoVM vm = new PreviewinfoVM();

                        string insuredname = Session["insuredname"].ToString();
                        vm.theclock.Insuredname = insuredname;
                        vm.theclock.Chasis_number = Session["Chasis_number"].ToString();
                        vm.theclock.Colour = Session["Colour"].ToString();
                        vm.theclock.Engine_number = Session["Engine_number"].ToString();
                        vm.theclock.Registeration_number = Session["registrationnumber"].ToString();
                        vm.theclock.Usage = Session["Usage"].ToString();
                        vm.theclock.Vehicle_Insurance_class = Session["Vehicle_Insurance_class"].ToString();
                        vm.theclock.Vehicle_make = Session["vehiclemake"].ToString();
                        vm.theclock.Vehicle_model = Session["vehiclemodel"].ToString();
                        vm.theclock.Vehicle_year = Session["vehicleyear"].ToString();
                    vm.theclock.Track_comp_name = Session["track_comp_name"].ToString();
                    vm.theclock.Track_comp_address = Session["track_comp_address"].ToString();
                    vm.theclock.Track_comp_phone = Session["track_comp_phone"].ToString();
                    return View(vm);

                    case "HL":
                        PreviewinfoVM info = new PreviewinfoVM();
                        return View(info);
                    default:
                        break;
                }

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Previewinfo(PreviewinfoVM vm)
        {
            var type = Session["producttype"].ToString();
            switch (type)
            {
                case "TC":
                    //pass parameters from session to the clock view model for save and email 
                    string insuredname = Session["insuredname"].ToString();
                    vm.theclock.Insuredname = insuredname;
                    vm.theclock.Chasis_number = Session["Chasis_number"].ToString();
                    vm.theclock.Colour = Session["Colour"].ToString();
                    vm.theclock.Engine_number = Session["Engine_number"].ToString();
                    vm.theclock.Registeration_number = Session["registrationnumber"].ToString();
                    vm.theclock.Usage = Session["Usage"].ToString();
                    vm.theclock.Vehicle_Insurance_class = Session["Vehicle_Insurance_class"].ToString();
                    vm.theclock.Vehicle_make = Session["vehiclemake"].ToString();
                    vm.theclock.Vehicle_model = Session["vehiclemodel"].ToString();
                    vm.theclock.Vehicle_year = Session["vehicleyear"].ToString();
                    vm.theclock.Track_comp_name = Session["track_comp_name"].ToString();
                    vm.theclock.Track_comp_address = Session["track_comp_address"].ToString();
                    vm.theclock.Track_comp_phone = Session["track_comp_phone"].ToString();
                    var res = new Cls_Policies().Inserttheclockpolicy(vm.theclock);
                    if(res.ErrorCode == 0)
                    {
                        //send email
                        ModDrive mod = new ModDrive();
                        var user = new Cls_Registration().GetuserbyID(mod.Getcurrentuser());
                         //string body = mod.PopulateBodyproductsuccessful(mod.Getcurrentuserfirstname(), "The Clock");
                            //mod.SendHtmlFormattedEmail(user.Email, "", "Your Policy has been created!", body);
                        TempData["success"] = "true";
                        TempData["message"] = "Your Policy registration has been successfull. You will get notified within 1-3 days to complete your purchase as soon as all background checks has been completed. You can always view the status and progress of your policies here.";
                       return RedirectToAction("policies");

                    }
                    else
                    {
                        return Redirect("/Home/error");
                    }
                    

                case "HL":
                    //theclockVM vm = new theclockVM();
                    //vm.Insuredname = Session["insuredname"].ToString();
                    //vm.Chasis_number = Session["Chasis_number"].ToString();
                    //vm.Colour = Session["Colour"].ToString();
                    //vm.Engine_number = Session["Engine_number"].ToString();
                    //vm.Registeration_number = Session["registrationnumber"].ToString();
                    //vm.Usage = Session["Usage"].ToString();
                    //vm.Vehicle_Insurance_class = Session["Vehicle_Insurance_class"].ToString();
                    //vm.Vehicle_make = Session["vehiclemake"].ToString();
                    //vm.Vehicle_model = Session["vehiclemodel"].ToString();
                    //vm.Vehicle_year = Session["vehicleyear"].ToString();
                    break;


            }
            return View();
        }
        public ActionResult Policies()
        {
            ModDrive mod = new ModDrive();
            var Policies = new Cls_Policies().Getpolicybyuser(mod.Getcurrentuser());

            return View(Policies);
        }

        public ActionResult Sidepoliciesview()
        {
            ModDrive mod = new ModDrive();
            var Policies = new Cls_Policies().Getpolicybyuser(mod.Getcurrentuser());

            return PartialView("_Sideofficenav", Policies);
        }
        public ActionResult Transactions()
        {
            return View();
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return Redirect("/");
        }


        public JsonResult GetvehicleMakeBrand(string makeid)
        {
            List<VehMakeBrand> brands = new Cls_Vehiclemakebrands().GetvehiclebrandByMake(makeid);

            return Json(brands, JsonRequestBehavior.AllowGet);
        }
    }
}